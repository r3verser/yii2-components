<?php

/*
 * This file is part of the yii2.local package.
 *
 * (c) Artem Voitko <r3verser@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\components;

use yii\base\Behavior;
use yii\web\Cookie;

/**
 * Class LocaleBehavior
 * @package app\components
 * @author  Artem Voitko <r3verser@gmail.com>
 */
class LocaleBehavior extends Behavior
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            \yii\web\Application::EVENT_BEFORE_REQUEST => 'beforeRequest'
        ];
    }

    /**
     * Callback
     */
    public function beforeRequest()
    {
        $language = explode('/', \Yii::$app->request->pathInfo)[0];
        if (!isset(\Yii::$app->params['availableLocales'][$language])) {
            \Yii::$app->language = \Yii::$app->params['defaultLocale'];
        } else {
            \Yii::$app->language = $language;
            \Yii::$app->session->set('language', $language);
        }
    }

}