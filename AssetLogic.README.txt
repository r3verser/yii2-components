        'assetLogic'           => [
            'class'   => '\app\components\AssetLogic',
            'bundles' => [
                [
                    '\yii\bootstrap\BootstrapAsset' => [
                        'on'     => function () {
                            return Yii::$app->controller->action->id == 'about';
                        },
                        'config' => [
                            'js'  => [],
                            'css' => [],
                        ],
                    ]
                ],
                [
                    \yii\bootstrap\BootstrapAsset::className() => [
                        'on'     => function () {
                            return (Yii::$app->getRequest()->queryString == 'q=1');
                        },
                        'config' => [
                            'js'  => [],
                            'css' => [],
                        ],
                    ]
                ],
                [
                    '\app\assets\AppAsset' => [
                        'on'     => function () {
                            return (Yii::$app->getRequest()->queryString == 'q=1');
                        },
                        'config' => [
                            'js'  => [],
                            'css' => [],
                        ],
                    ]
                ]
            ]
        ],