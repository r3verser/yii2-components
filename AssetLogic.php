<?php

/*
 * This file is part of the timer2.local package.
 *
 * (c) Artem Voitko <r3verser@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\components;


use yii\base\InvalidParamException;
use yii\base\Object;
use yii\web\View;

/**
 * Class AssetLogic
 * Enables custom configuration for asset bundles depending on user logic
 * @package Yii2
 * @author  Artem Voitko <r3verser@gmail.com>
 */
class AssetLogic extends Object
{
    /**
     * @var string Event when new bundle config applies
     */
    public $applyOn = View::EVENT_BEFORE_RENDER;
    /**
     * @var array Custom Asset Bundles configuration
     */
    protected $arraysOfBundles = [];

    /**
     * $arraysOfBundles setter
     * @param $bundles
     */
    public function setBundles($bundles)
    {
        $this->arraysOfBundles = $bundles;
    }

    /**
     * Configure asset bundles
     */
    public function init()
    {
        \Yii::$app->getView()->on($this->applyOn, function () {
            foreach ($this->arraysOfBundles as $bundles) {
                foreach ($bundles as $bundleClassName => $params) {
                    if (!is_callable($params['on'])) {
                        throw new InvalidParamException(\Yii::t('app', 'Callable expected in "on" configuration parameter. The "{on}" given.', ['on' => $params['on']]));
                    }
                    if (call_user_func($params['on'])) {
                        \Yii::$app->getView()->getAssetManager()->bundles[$bundleClassName] = $params['config'];
                    }
                }
            }
        });
    }

}


