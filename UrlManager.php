<?php

/*
 * This file is part of the yii2.local package.
 *
 * (c) Artem Voitko <r3verser@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\components;


/**
 * Class UrlManager
 * @package app\components
 * @author  Artem Voitko <r3verser@gmail.com>
 */
class UrlManager extends \yii\web\UrlManager
{

    public function createUrl($params)
    {
        if (\Yii::$app->language !== \Yii::$app->params['defaultLocale'] && !isset($params['language'])) {
            $params['language'] = \Yii::$app->language;
        }
        return parent::createUrl($params);
    }

}